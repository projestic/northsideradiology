<?php
/**
 * Template Name: Contact Us Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */
global $wp_query;
get_header(); get_template_part('head-section');?>

        <section class="section-physicians section-contact">
            <div class="container">
                <div class="breadcrumbs">
                    <?php custom_breadcrumbs(); ?>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="abstract-text">
                            <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();
                           echo get_the_content();

                        // End the loop.
                        endwhile;
                        ?>
                        </div>

                        <div class="form-box" id="contact-form-block">
                           <?php echo do_shortcode ('[contact-form-7 id="35" title="main_form"]')?>
                            <span class="privacy_block">When you submit this form, you agree to our <a href="#" class="privacy">privacy policy.</a></span>
                        </div>

                        <div class="user-text">
                            <?php the_field('second_block_text')?>
                        </div>
                        <div class="block-title">
                            <span class="text"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon.png"></span>
                        </div>
                        <div class="related_pages_block">
                            <h4>Related Pages</h4>
                            <ul class="related_pages_list">
                               <?php

                                // check if the repeater field has rows of data
                                if( have_rows('related_pages_repeater') ):

                                    // loop through the rows of data
                                    while ( have_rows('related_pages_repeater') ) : the_row();
                                        $post_object = get_sub_field('related_post');
                                        $post = $post_object;
                                       setup_postdata( $post ); ?>
                                <li><span class="title"><?php the_title();?></span><a href="<?php the_permalink();?>" class="more-btn">View ></a></li>
                                <?php wp_reset_postdata();

                                    endwhile;

                                else :

                                    // no rows found

                                endif;

                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
<!--                        --><?php
//                        if ( function_exists('dynamic_sidebar') )
//                            dynamic_sidebar('contact-sidebar');
//                    ?>
                        <?php dynamic_sidebar(); ?>
                    </div>
                </div>


            </div>
        </section>
       
    <?php get_footer(); ?>