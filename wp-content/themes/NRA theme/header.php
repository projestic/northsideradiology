<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--    <script src="https://maps.googleapis.com/maps/api/js"></script>-->
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>






</head>

<body <?php body_class(); ?>>
<!-- Header -->
<header class="main-header clearfix down">
    <h1 class="logo">
        <a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png" alt="Logo"></a>
    </h1>
    <div class="main-menu-wrp">
       <?php wp_nav_menu(array( 'menu_class' => 'main-menu-list', 'menu' => 'Main menu')); ?>
    </div>
    <div class="mobile_menu_wrp hidden-md hidden-lg"><a href="#" class="mobile_menu closed"><span class="menu_open">Menu</span><span class="menu_close">Close</span></a>
    </div>
    <div class="menu_list_holder">
        <?php wp_nav_menu(array( 'menu_class' => 'mobile_menu_list', 'menu' => 'Main menu')); ?>
    </div>
</header>
<!-- end Header -->