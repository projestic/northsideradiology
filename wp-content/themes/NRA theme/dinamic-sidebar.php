	<?php 
		if(have_rows('sidebar_menus')):
			while ( have_rows('sidebar_menus') ) : the_row();
	?>
<div class="homepage-widget-block">
		<h4><?php the_sub_field('menu_title');?></h4>
		<?php 
			if (have_rows('menu_links')) :?>
			<ul>
				<?php
						while ( have_rows('menu_links') ) : the_row();
				?>
					<li><a href="<?php the_sub_field('menu_links');?>"><?php the_sub_field('menu_link_title');?></a></li>
				<?php endwhile; ?>
			</ul>
		<?php endif;?>
</div>
	<?php endwhile; endif;?>
<?php
	global $wp_query;
	$page_id = $wp_query->post->ID;
	if ($page_id == 79) : 
?>
<div class="homepage-widget-block">
	<h4>Cases of the month</h4>
	<div class="cases-select-holder">
		<select class="cases-select" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
			<option value="0" disabled selected>Select a month</option>
			<?php
				$type = 'cases';
				$args3=array(
				  'post_type' => $type,
				  'post_status' => 'publish',
				  'posts_per_page' => -1,
				  'caller_get_posts'=> 1,
				  'order' => 'ASC'
				);
				$my_query = null;
				$my_query = new WP_Query($args3);
				if( $my_query->have_posts() ) {
				  while ($my_query->have_posts()) : $my_query->the_post(); ?>
				    <option value="<?php the_permalink();?>"><?php the_title();?></option>
				    <?php
				  endwhile;
				}
				wp_reset_query();  // Restore global post data stomped by the_post().
			?>
		</select>
		<span class="select-arrow"></span>
	</div>
	
</div>
<?php endif;?>
<?php if(get_field('show_contact_form')): ?>
	<div class="homepage-widget-block">
		<div class="contact_form">
			<?php echo do_shortcode('[contact-form-7 id="1511" title="sidebar form"]');?>
		</div>
	</div>
<?php endif;?>
<?php if(get_field('show_uterine_post')): ?>
	<div class="homepage-widget-block">
		<div class="post_link_image">
		    <h3>Uterine Fibroid Embolization</h3>
		    <a href="#" class="btn main-btn">View</a>
		</div>
	</div>
<?php endif;?>