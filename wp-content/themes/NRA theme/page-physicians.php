<?php
/**
 * Template Name: Physicians Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <?php
    /*$specialties_info = get_field_object('field_56af3644ebf79');
    $specialties_arr = $specialties_info['choices'];*/

    $cats = get_terms(array('taxonomy' =>'physicians_category', 'hide_empty' => true));
    $args = array(
            'post_type' => 'physicians',
            'staff-type' => $type,
            'no_paging' => true,
            'posts_per_page' => -1,
            'meta_key'       => 'user_last_name',
            'orderby'        => 'meta_value',
            'order'          => 'ASC'
        );

    ?>
    <?php get_template_part('head-section'); ?>
    <section class="section-physicians">
        <div class="container">
            <div class="breadcrumbs">
                <?php custom_breadcrumbs(); ?>
            </div>
                
            <div class="abstract-text">
                <?php the_content(); ?>
            </div>             

            <div class="filter-area">
                <?php  

                    $alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

                ?>

                <div class="sort-lastname_cont">
                    <h5>Sort by last name: </h5>
                    <label class="filter-last-name-all">
                        <input type="radio" name="physiciansFirstLetter" value="" class="j-filter-last-name" checked>
                        <span>View All</span>
                    </label>
                    <div class="lastname-filter">

                        <?php foreach ($alphabet as $letter) : ?>
                            
                            <label>
                                <input type="radio" name="physiciansFirstLetter" value="<?php echo $letter; ?>" class="j-filter-last-name">
                                <span><?php echo $letter; ?></span>
                            </label>

                        <?php endforeach; ?>
                        
                    </div>

                </div>

                <div class="sort-speciality_cont">
                    <h5>Sort by speciality:</h5>
                    
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" id="dropdown-list-categories" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span id="selected" class="selected-item">Choose option</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdown-list-categories">
                            <li><a href="javascript:void(0)" class="filter-spec" data-spec="" title="All">View all</a></li>
                            <?php foreach ($cats as $cat) : ?>
                                <li><a href="javascript:void(0)" class="filter-spec" data-spec="<?php echo $cat->term_id; ?>" title="<?php echo $cat->name; ?>"><?php echo $cat->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="media-box">
                <?php the_field('media_block'); ?>
            </div>
            <div class="profile-list" id="physicians-list">
                <div class="row row-physicians">
                    <h2 class="notfound">Sorry nothing found for selected order. Please choose another ordering.</h2>
                    <?php

                    $loop = new WP_Query($args);
  
                    if ($loop->have_posts()) :
                        while ($loop->have_posts()) : $loop->the_post();
                            get_template_part('physicians', 'item');
                        endwhile;
                        wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>
    <script>

        jQuery(function ($) {

            var filteredCat,
                filteredLetter,
                $physiciansEmptyText = $('#physicians-list .notfound'),
                $physiciansList = $('#physicians-list .profile_cont');

            $('.dropdown-menu li a').click(function(e) {
                e.preventDefault();
                if (!$(this).hasClass('active')) {
                    $('.dropdown-menu li a').removeClass('active');
                    $(this).toggleClass('active');
                    filteredCat = '' + $(this).data('spec');

                    sortPhysicians($physiciansList, filteredCat, filteredLetter);
                }                
            });

            function sortPhysicians(obj, cat, letter){ 

                var result = obj.filter(function() { 

                    var cats = ('' + $(this).data('user-cat')).split(',');

                    return (!letter || $(this).data('user-lastname') == letter) && (!cat || cats.indexOf(cat) != -1);
                });

                obj.hide();
                result.show();

                if(result.length){
                    $physiciansEmptyText.hide();
                } else {
                    $physiciansEmptyText.show();
                }
            }

            $('.j-filter-last-name').on('change', function(){
                filteredLetter = $(this).val();
                sortPhysicians($physiciansList, filteredCat, filteredLetter);
            });

        });
    </script>
<?php get_footer(); ?>
