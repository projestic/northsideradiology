<?php
/**
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header();global $wp_query; ?>
	<section class="head-section">
		<div class="container-fluid">
			<h1 class="page-title">Case of the Month</h1>
		</div>
	</section>
    <section class="section-physicians section-patient">
        <div class="container">
			<div class="breadcrumbs">
				<?php custom_breadcrumbs(); ?>
			</div>
            <div class="row">
                <div class="col-md-9">
                    <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post(); 
                    ?>
                    <div class="case-holder">
                    	<h3><?php the_title(); ?></h3>

                    	<h3>Symptoms</h3>
                    	<p class="symptoms-text">
                    		<?php echo wp_strip_all_tags(get_field('symptoms'));?>
                    	</p>
                    	<div class="screens-holder">
                    		<?php 
                    			if( have_rows('screens_repeater') ):
                    				while ( have_rows('screens_repeater') ) : the_row();	
                    		?>
                    		<a href="<?php the_sub_field('screen');?>" class="fancybox" rel="group"><img src="<?php the_sub_field('screen');?>"></a>
                    		<?php endwhile; endif;?>
                    	</div>
                    	<h3 class="diagnosis-title">
                    		Diagnosis
                    	</h3>
                    	<p class="diagnosis-text">
                    		<?php echo wp_strip_all_tags(get_field('diagnosis'));?>
                    	</p>
                    	<?php next_post_link('%link','<span class="read-more">< View Previous Case</span>'); ?><span style="width: 20px; display: inline-block;"></span><?php previous_post_link('%link','<span class="read-more">View Next Case ></span>'); ?> 
                    </div>
                    <?php $content =  get_the_content();  ?>

                    <?php endwhile; ?>
                    
                    <?php
                        if( have_rows('related_pages') ):
                    ?>
                        <div class="block-title">
                            <span class="text"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon.png"></span>
                        </div>
                        <div class="related_pages_block">
                            <h4>Related Pages</h4>
                            <ul class="related_pages_list">
                                <?php

                                    // check if the repeater field has rows of data
                                    if( have_rows('related_pages') ):

                                        // loop through the rows of data
                                        while ( have_rows('related_pages') ) : the_row();
                                            $post_object = get_sub_field('related_post');
                                            $post = $post_object;
                                           setup_postdata( $post ); ?>
                                    <li><span class="title"><?php the_title();?></span><a href="<?php the_permalink();?>" class="more-btn">View ></a></li>
                                    <?php wp_reset_postdata();

                                        endwhile;

                                    else :

                                        // no rows found

                                    endif;

                                    ?>
                            </ul>
                        </div>
                    <?php endif;?>
                </div>
                <div class="col-md-3">
                    <div class="homepage-widget-block">
						<h4>Cases of the month</h4>
						<div class="cases-select-holder">
							<select class="cases-select" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
								<option value="0" disabled selected>Select a month</option>
								<?php
									$type = 'cases';
									$args3=array(
									  'post_type' => $type,
									  'post_status' => 'publish',
									  'posts_per_page' => -1,
									  'caller_get_posts'=> 1,
									  'order' => 'ASC'
									);
									$my_query = null;
									$my_query = new WP_Query($args3);
									if( $my_query->have_posts() ) {
									  while ($my_query->have_posts()) : $my_query->the_post(); ?>
									    <option value="<?php the_permalink();?>"><?php the_title();?></option>
									    <?php
									  endwhile;
									}
									wp_reset_query();  // Restore global post data stomped by the_post().
								?>
							</select>
							<span class="select-arrow"></span>
						</div>
					</div>
					<div class="homepage-widget-block">
    						<h4>About</h4>
    						<div class="about-text">
    							<?php the_field('cases_about_text', 'option')?>
    						</div>
					</div>
                </div>
            </div>
        </section>
<?php get_footer(); ?>
