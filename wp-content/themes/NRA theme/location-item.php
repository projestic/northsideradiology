<div class="profile-block hospitals-block">
    <div class="row">
        <div class="col-sm-12">
            <div class="profile-info">
                <div class="col profile-name">
                    <h3 class="title"><?php the_field('name', $post_id);?></h3>
                    <p class="adress"><?php the_field('house', $post_id);?> <?php the_field('road', $post_id);?><br><?php the_field('city', $post_id);?>, <?php the_field('state', $post_id);?> <?php the_field('zip', $post_id);?></p>
                        <a href="https://www.google.com/maps/place/<?php the_field('house', $post_id);?>+<?php the_field('road', $post_id);?>+<?php the_field('city', $post_id);?>+<?php the_field('state', $post_id);?>+<?php the_field('zip', $post_id);?>" target="_blank" class="read-more show_map" data-address="<?php the_field('house', $post_id);?> <?php the_field('road', $post_id);?> <?php the_field('city', $post_id);?> <?php the_field('state', $post_id);?>" data-zip="<?php the_field('zip', $post_id);?>">Open in Google Maps ></a>
                </div>
                <div class="col profile-specs">
                    <div class="title">PROCEDURES</div>
                    <p><?php the_field('procedures', $post_id); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
