<?php

/**
 * Template Name: Patient Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */
global $wp_query;
get_header();
get_template_part('head-section'); ?>

    <section class="section-physicians section-patient">
        <div class="container">
            <div class="breadcrumbs">
                <?php custom_breadcrumbs(); ?>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="abstract-text">
                        <?php
                        // Start the loop.
                        while (have_posts()) : the_post();
                            echo get_the_content();

                            // End the loop.
                        endwhile;
                        ?>
                    </div>
                    <?php if (get_field('media_box')) { ?>
                        <div class="media-box">
                            <?php the_field('media_box'); ?>
                        </div>
                    <?php } ?>
                    <?php
                    if (is_page(7)) {
                        ?>
                        <div class="procedures-holder clearfix">
                            <h3 class="procedures-title">Procedures</h3>
                            <div class="procedures-wrp clearfix">
                                <?php
                                $categories = get_categories('child_of=17&hide_empty=0');
                                foreach ($categories as $category) :
                                    $categoryID = $category->cat_ID;
                                    if ($categoryID !== 26) :
                                        ?>
                                        <div class="single-category">
                                            <h4><?php echo $category->name; ?></h4>
                                            <ul>
                                                <?php
                                                $args = array('post_type' => 'procedures', 'cat' => $categoryID);
                                                $loop = new WP_Query($args);
                                                while ($loop->have_posts()) : $loop->the_post();
                                                    ?>
                                                    <li>
                                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                    </li>
                                                <?php endwhile;
                                                wp_reset_query();
                                                ?>
                                            </ul>
                                        </div>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                                </ul>
                            </div>
                            <div class="right-category">
                                <h4><?php echo get_the_category_by_ID(26); ?></h4>
                                <ul>
                                    <?php
                                    $args2 = array('post_type' => 'procedures', 'cat' => 26);
                                    $loop2 = new WP_Query($args2);
                                    while ($loop2->have_posts()) : $loop2->the_post();
                                        ?>
                                        <li>
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </li>
                                    <?php endwhile;
                                    wp_reset_query();
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (get_field('second_text_block')) { ?>
                        <div class="user-text">
                            <?php the_field('second_text_block'); ?>
                        </div>
                    <?php } ?>
                    <?php
                    if (have_rows('related_pages')):
                        ?>
                        <div class="block-title">
                            <span class="text"><img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon.png"></span>
                        </div>
                        <div class="related_pages_block">
                            <h4>Related Pages</h4>
                            <ul class="related_pages_list">
                                <?php

                                // check if the repeater field has rows of data
                                if (have_rows('related_pages')):

                                    // loop through the rows of data
                                    while (have_rows('related_pages')) : the_row();
                                        $post_object = get_sub_field('related_post');
                                        $post = $post_object;
                                        setup_postdata($post); ?>
                                        <li><span class="title"><?php the_title(); ?></span><a
                                                href="<?php the_permalink(); ?>" class="more-btn">View ></a></li>
                                        <?php wp_reset_postdata();

                                    endwhile;

                                else :

                                    // no rows found

                                endif;

                                ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    <?php
                    //                        if ( function_exists('dynamic_sidebar') )
                    //                            dynamic_sidebar('locations-sidebar');
                    ?>
                    <?php dynamic_sidebar(); ?>
                </div>
            </div>


        </div>
    </section>
<?php get_footer(); ?>