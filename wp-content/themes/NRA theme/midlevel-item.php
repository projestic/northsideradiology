<div class="col-xs-12 col-sm-6 col-md-6 stretch-height">
    
    <div class="profile-block">
        <div class="col-top">
            <h3 class="title"><?php the_title();?></h3>

            <div class="image_holder">
            
                <?php get_template_part('profile-thumbnail', 'item'); ?>

            </div>

            <a href="<?php the_permalink();?>" title="View Full Profile" class="read-more">View Profile</a>

        </div>

        <div class="col-bottom">
            
            <div class="title">About</div>

            <?php 
                $textExerpt = get_field('midlevel_exerpt');

                if ($textExerpt) : 

                    echo getExcerpt($textExerpt);

                endif; 
            ?>

        </div>

    </div>

</div>