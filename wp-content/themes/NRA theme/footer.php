<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */
?>
<!-- Footer -->
<footer class="site-footer">
    <div class="small_grid">
        <div class="one_third visible-lg-inline-block">
            <a href="<?php echo get_site_url(); ?>" class="footer_logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer_logo.png" alt="Footer logo"></a>
            <?php while ( have_rows('links', 'options') ) : the_row(); ?>
            <p><?php the_sub_field('link'); ?></p>
            <?php endwhile; ?>
        </div>
        <div class="one_third hidden-lg">
            <h4>Menu</h4>
            <?php wp_nav_menu(array( 'menu' => 'Footer mobile menu', 'container' => false)); ?>
        </div>
        <div class="one_third">
            <h4>LOCATIONS</h4>
            <?php wp_nav_menu(array( 'menu' => 'Locations Footer 1', 'container' => false)); ?>
        </div>
        <div class="one_third">
            <h4>SPECIALITIES</h4>
            <?php wp_nav_menu(array( 'menu' => 'Specialties Footer 2', 'container' => false)); ?>
        </div>
    </div>
    <div class="copy">
        <?php the_field('copyright', 'options'); ?>
    </div>
</footer>
<!-- end Footer -->

<?php wp_footer(); ?>
<script>
jQuery(function($) {
    $('div.wpcf7-response-output').toggleClass( "wpcf7-response-output" );
});
</script> 
</body>
</html>
