<?php
/**
 * Template Name: New Locations Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

global $wp_query;
get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('head-section'); ?>

    <section class="section-physicians section-locations">
        <div class="container">
            <div class="breadcrumbs">
                <?php custom_breadcrumbs(); ?>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div><?php the_content(); ?></div>
                </div>
                <div class="col-md-3">
<!--                    --><?php
//                    if (function_exists('dynamic_sidebar'))
//                        dynamic_sidebar('patients-sidebar');
//                    ?>
                    <?php dynamic_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>
<?php
$args2 = array('post_type' => 'locations', 'posts_per_page' => -1);
$loop = new WP_Query($args2);
if ($loop->have_posts()) : ?>
    <div id="locations-listing" hidden>
        <?php while ($loop->have_posts()) : $loop->the_post();
            $post_id = get_the_ID(); ?>
            <p><?php $location = get_field('location_map');
                echo $location['lat'] . '|' . $location['lng'] . '|' . $location['address'] ?></p>
        <?php endwhile; ?>
    </div>
    <?php wp_reset_postdata(); endif; ?>
    <script>
        var zip = "";
        var procedure = "";
        var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
        jQuery(function ($) {
            $('#zip-code').keypress(function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    if (/^\d{5}$/.test(this.value) || this.value == '') {
                        zip = this.value;
                        load_locations(0);
                    } else {
                        alert('Invalid zip code');
                    }
                }

            });

            $('#procedure-sort').change(function () {
                if (this.value) {
                    if (this.value === ' ') {
                        procedure = this.value;
                        load_locations(1);
                        this.value === '';
                        $(this).find('option').first().attr('selected', true);
                    } else {
                        procedure = this.value;
                        load_locations(0);
                    }
                }
            });

            $('#locations-list').on('click', '#view-all', function (e) {
                e.preventDefault();
                load_locations(1);
                return false;
            });

            function load_locations(all) {return;
                var data = {'action': 'load_locations', zip: zip, procedure: procedure, show_all: all};
                jQuery.get(ajax_url, data, function (response) {
                    if (response) {
                        $('#locations-list').html(response);
                    } else {
                        $('#locations-list').html('<h3>There are no items</h3>');
                    }
                });
            }
        });
    </script>
<?php get_footer(); ?>