/*
 Third party
 */

//= ../vendor/jquery/dist/jquery.min.js
//= ../vendor/slick-carousel/slick/slick.min.js
//= ../vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js
//= ../vendor/fancybox/source/jquery.fancybox.pack.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/collapse.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/button.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/modal.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js


var APP = (function () {
    var my = {};
    //markers
    var markers = [];
    var map;
    //styles for map
    var styles = [{
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [{
            "color": "#f7f1df"
        }]
    }, {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [{
            "color": "#d0e3b4"
        }]
    }, {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi.medical",
        "elementType": "geometry",
        "stylers": [{
            "color": "#fbd3da"
        }]
    }, {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [{
            "color": "#bde6ab"
        }]
    }, {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ffe15f"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#efd151"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ffffff"
        }]
    }, {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "black"
        }]
    }, {
        "featureType": "transit.station.airport",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#cfb2db"
        }]
    }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
            "color": "#a2daf2"
        }]
    }];
    //Private variable for window width and resize
    var window_width = 0;
    // var geocoder = new google.maps.Geocoder();
    //variable scroll position
    var scroll = 0;
    var scrollDown = true;
    //function for check window width
    function check_window() {
        return window.innerWidth;
    }

    //start slider
    function main_slider(width) {
        if ($('.procedures-list').length) {
            if (width <= 991) {
                if (!($('.procedures-list').hasClass('slick-initialized'))) {
                    $('.procedures-list').slick({
                        dots: true,
                        autoplay: true
                    });
                }
            } else {
                if ($('.procedures-list').hasClass('slick-initialized')) {
                    $('.procedures-list').slick('unslick');
                }
            }
        }
    }

    //is mobile
    function isMobile() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return true;
        } else {
            return false;
        }
    }

    //Toggle menu
    function toggleMenu() {
        var status = $('.mobile_menu').hasClass('closed');
        if (status) {
            $('.mobile_menu').removeClass('closed').addClass('opened');
            if (window_width <= 991) {
                $('.main-header').addClass('opened');
            }
        } else {
            $('.mobile_menu').removeClass('opened').addClass('closed');
            if (window_width <= 991) {
                $('.main-header').removeClass('opened');
            }
        }
    }
    //function for fix menu-height
    function checkMenuHeight() {
        var menu = $('.menu_list_holder');
        var windowHeight = $(window).height();
        var headerHeight = $('.main-header').height();
        var menuHeight = windowHeight - 62;
        menu.height(menuHeight);
    }
    //init map
    function initMap() {
        if ($('.map_holder').length) {
            var mapDiv = document.getElementById('map');
            map = new google.maps.Map(mapDiv, {
                center: {
                    lat: 33.9091928,
                    lng: -84.3562401
                },
                zoom: 15
            });
            map.setOptions({
                styles: styles
            });
            initMarkers();
            var bounds = fitMap();
            map.fitBounds(bounds);
        }
    }

    //function get selected option for procedures
    function getValues(el) {
        console.log($(el + " option:checked").val());
    }

    //new version geoloc
    function geoloc() {
        var locArr = [];
        var locStrings;
        if ($('#locations-listing').length) {
            locStrings = $('#locations-listing').find('p');
            $('#locations-listing').remove();
            for (var i = 0; i < locStrings.length; i++) {
                var text = $(locStrings[i]).text();
                var lat, lng, title;
                lat = text.split('|')[0];
                lng = text.split('|')[1];
                title = text.split('|')[2];
                locArr.push({
                    'title': title,
                    'lat': lat,
                    'lng': lng
                });
            }
        }
        return locArr;
    }

    //function for create markers
    function createMarker(obj) {
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(obj.lat), lng: parseFloat(obj.lng)},
            title: obj.title
        });
        markers.push(marker);
        marker.addListener('click', function () {
            clickMarker(obj.title);
        });
        return marker;
    }

    //function for gather markers
    function initMarkers() {
        var markerdata = geoloc();
        for (var i = 0; i < markerdata.length; i++) {
            var marker = createMarker(markerdata[i]);
            marker.setMap(map);
        }
    }

    //function for change header
    function headerDown(dir) {
        if (dir && !(isMobile())) {
            var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
            window.requestAnimationFrame = requestAnimationFrame;
            var header = $('.main-header');
            var header_height = header.outerHeight();
            if (dir === 'down') {
                header.addClass('down').removeClass('up');
                header.next().css({
                    'margin-top': header_height
                });
                header.css({
                    'position': 'fixed',
                    'top': -header_height,
                    'left': 0,
                    'width': '100%',
                    'background': '#ffffff'
                });
                header.finish();
                requestAnimationFrame(function () {
                    header.animate({
                        'top': 0
                    }, 300);
                })
            } else if (dir === 'up') {
                header.removeClass('down').addClass('up');
                header.finish();
                requestAnimationFrame(function () {
                    header.animate({
                        'top': -header_height
                    }, 300);
                })
                header.next().css({
                    'margin-top': 0
                });
                header.css({
                    'position': 'static',
                });
            }
        }


    }

    // //function initmarkers
    // function initMarkers() {
    //     if ($('.show_map').length) {
    //         var addressList = $('.show_map');
    //         var addressaArr = [];
    //         for (var i = 0; i < addressList.length; i++) {
    //             var tempAddress = $(addressList[i]).attr('data-address');
    //             addressaArr.push(tempAddress);
    //         }
    //         geocodeMarker(addressaArr);
    //     }
    // }

    //function init zipcode
    function initZip() {
        if ($('.zipcode').length) {
            $('.zipcode').inputmask({
                mask: "99999",
                placheholder: 'ZIP CODE'
            });
        }
    }

    $('.zipcode').attr('placeholder', 'ZIP CODE');

    //function for sort locations
    function sortLocations() {
        var statusSort = $('.procedure_sort').parent().hasClass('up');
        if (statusSort) {
            $('.procedure_sort').parent().removeClass('up').addClass('down');
        } else {
            $('.procedure_sort').parent().removeClass('down').addClass('up');
        }
    }

    //function geocoder
    function geocodeAddress(address) {
        var geocoder = new google.maps.Geocoder();
        var mapDiv = document.getElementById('map');
        var resultsMap = new google.maps.Map(mapDiv, {
            center: {
                lat: 33.9091928,
                lng: -84.3562401
            },
            zoom: 15
        });
        resultsMap.setOptions({
            styles: styles
        });
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location
                });
            } else {
                console.error('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    //function geocodemarker
    function geocodeMarker(addressArr) {
        var geocoder = new google.maps.Geocoder();
        var codeAddress = function (zip) {
            geocoder.geocode({'address': zip}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: zip
                    });
                    markers.push(marker);
                    marker.addListener('click', function () {
                        clickMarker(zip);
                    });
                    fitMap();
                } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    setTimeout(function () {
                        codeAddress(zip);
                    }, 250);
                } else {
                    console.error("Geocode was not successful for the following reason: " + status);
                }
            });
        }
        for (var i = 0; i < addressArr.length; i++) {
            codeAddress(addressArr[i]);
        }
        // for (var i = 0; i < addressArr.length; i++) {
        //     geocoder.geocode({
        //         'address': addressArr[i]
        //     }, function (results, status) {
        //         if (status === google.maps.GeocoderStatus.OK) {
        //             console.log(status);
        //             var marker = new google.maps.Marker({
        //                 map: map,
        //                 position: results[0].geometry.location
        //             });
        //             markers.push(marker);
        //             fitMap();
        //         } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
        //             setTimeout(function() {
        //                 codeAddress(zip);
        //             }, 250 );
        //         } else {
        //             alert("Geocode was not successful for the following reason: " + status);
        //         }
        //     });
        // }
    }

    //function for fit map to markers
    function fitMap() {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        return bounds;
    }

    //function check where we scrolling
    function headerScroll(tempScroll) {
        var header = $('.main-header');
        if ((tempScroll - scroll) < 0) {
            if (!(header.hasClass('down'))) {
                header.addClass('down').removeClass('up');
            }
        } else {
            header.removeClass('down').addClass('up');
        }
    }

    //function for click marker to open new window with direction to point
    function clickMarker(url) {
        var newWin = window.open('https://maps.google.com?saddr=Current+Location&daddr=' + url);
        newWin.focus();
    }

    //function clear markers
    function clearMarker() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }

    //function for init Fancybox
    function initFancybox() {
        $('.fancybox').fancybox({});
    }

    //function for click on popup
    function directionClick(el) {
        console.debug('clicked', el);
    }
    //function for show all locations
    function showAllLocations() {
        $('.profile-list').removeClass('showfirst');
        $('.view-all-block').remove();
    }

    //all listeners
    function listeners() {
        window_width = check_window();
        main_slider(window_width);
        $(window).on('resize', function () {
            window_width = check_window();
            main_slider(window_width);
        });
        $('.mobile_menu').on('click', function (e) {
            e.preventDefault();
            toggleMenu();
        });
        $('.mobile_menu_list').on('click', 'a', function (e) {
            toggleMenu();
        });

        //listen ajax calls
        $(document).ajaxSuccess(function () {
            if ($('#map').length) {
                // clearMarker();
                // initMarkers();
                // fitMap();
            }
            showAllLocations();
        });
        // $('.procedure_sort').on('click blur', function () {
        //     sortLocations();
        // });
        // $('.procedure_sort').on('change', function () {
        //     getValues('.procedure_sort');
        // });
        //listener for change sort
        $('.menuDropDown').on('click', 'a', function () {
            if (!($(this).hasClass('selected'))) {
                $('.sort-btn').addClass('hidden');
                $('.reset-btn').addClass('visible');
            }
            showAllLocations();
        });
        $('.reset-btn').on('click', function () {
            $(this).removeClass('visible');
            $('.sort-btn').removeClass('hidden');
            showAllLocations();
        });
        $(document).on('click', '.get-direction-btn', function (e) {
            e.preventDefault();
            console.log('clicked');
            // directionClick(this);
        });
        //If we scrolling and mobile menu is opened we turn off menu
        $(window).on('scroll', function (e) {
            var tempScroll = $(window).scrollTop();
            if(tempScroll < 0){
                return false;
            }
            headerScroll(tempScroll);
            scroll = tempScroll;
            if ($('.main-header').hasClass('opened')) {
                toggleMenu();
            }
        });
        $('#view-all').on('click', function (e) {
            e.preventDefault();
            showAllLocations();
        });
        $('.doPrettySearch').on('click', function () {
            showAllLocations();
        });
        $(window).on('resize orientationchange',function () {
            checkMenuHeight();
        });
    }

    $('.dropdown-menu a').click(function(){
        $('#selected').text($(this).text());
    });

    $('.procedure_sort').mouseover(function () {
        $(this).css({
            opacity: .8,
            background: '#c0c4cc',
            color: '#fff',
            transition: 'all .1s'
        });
    });

    $('.procedure_sort').mouseout(function () {
        $(this).css({
            background: '#343840',
            opacity: 1
        });
    });

    my.init = function () {
        listeners();
        initMap();
        initZip();
        initFancybox();
        checkMenuHeight();
        console.info('Started!');
    }
    return my;
}());
$(document).ready(function () {
    APP.init();
});