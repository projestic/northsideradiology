<?php
/**
 * The template for displaying the template
 *
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */
?>
<section class="head-section">
	<div class="container-fluid">
		<h1 class="page-title"><?php echo get_the_title( $wp_query->post->ID)?></h1>
	</div>
</section>