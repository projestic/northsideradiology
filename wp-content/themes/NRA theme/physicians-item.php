<?php
    $physicians_categories = get_the_terms($post->ID, 'physicians_category') ?: [];
    $last_name = get_field('user_last_name') ?: '';
    $firstLetter = substr($last_name, 0, 1);

    if(!empty($physicians_categories)):
        foreach ($physicians_categories as $key => $physicians_category) {
            $cat_ids[] =  $physicians_category->term_id;
        }
    endif;
?>

<div class="col-xs-12 col-sm-6 col-md-6 profile_cont stretch-height" data-user-lastname="<?php echo $firstLetter; ?>" data-user-cat="<?php echo join(',', $cat_ids); ?>">
    
    <div class="profile-block">

        <div class="col-top">
            
            <h3 class="title"><?php the_title();?></h3>

            <a href="<?php the_permalink();?>" class="image_holder">

                <?php get_template_part('profile-thumbnail', 'item'); ?>

            </a>

            <a href="<?php the_permalink();?>" title="View Full Profile" class="read-more">View Profile</a>

        </div>

        <div class="col-bottom">
            
            <div class="title">Specialties</div>
            <p><?php the_field('specialties')?></p>

        </div>

    </div>

</div>