<?php 

    $image = get_field('profile_photo');

    // vars
    $title = $image['title'];
    $alt = $image['alt'];

    // thumbnail
    $size = 'post-physicians-size';
    $thumb = $image['sizes'][ $size ];
    $width = $image['sizes'][ $size . '-width' ];
    $height = $image['sizes'][ $size . '-height' ];

?>

<?php
    if(get_field('profile_photo')) :
?>
    
    <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />

<?php else : ?>

    <?php
        if(get_field('default_profile_image')) :
    ?>
        
        <?php if(get_field('default_profile_image') == 'Female Sans') :?>
            
        
            <div class="image_stub" style="background: #31363e url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/female.jpg') no-repeat center -3px;"></div>

        <?php else : ?>

            <div class="image_stub" style="background: #31363e url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/male.jpg') no-repeat center -3px;"></div>

        <?php endif; ?>

    <?php endif;?>

<?php endif;?>