<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header();global $wp_query;
get_template_part('head-section'); ?>
<section class="section-physicians">
        <div class="container">
            <div class="breadcrumbs">
                <ul>
                    <li>
                        <a href="<?php echo get_site_url(); ?>" title="">Home</a>
                    </li>
                    <li>
                        <span><?php echo get_the_title( $wp_query->post->ID)?></span>
                    </li>
                </ul>
            </div>
            <h1>Page not found!</h1>
        </div>
    </section>

<?php get_footer(); ?>
