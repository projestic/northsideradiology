<?php
/**
 * NRA functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since NRA 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * NRA only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'nra_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since NRA 1.0
 */
function nra_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on nra, use a find and replace
	 * to change 'nra' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'nra', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 250, 313, true );
	add_image_size('post-physicians-size', 250, 313, array('center', 'center'));

	add_theme_support( 'post-thumbnails' ); 
	set_post_thumbnail_size( 1200, 9999 );
	// check if the post has a Post Thumbnail assigned to it.

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'nra' ),
		'social'  => __( 'Social Links Menu', 'nra' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = nra_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'nra_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', nra_fonts_url() ) );
}
endif; // nra_setup
add_action( 'after_setup_theme', 'nra_setup' );

/**
 * Register widget area.
 *
 * @since NRA 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function nra_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'nra' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'nra' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'nra_widgets_init' );

if ( ! function_exists( 'nra_fonts_url' ) ) :
/**
 * Register Google fonts for NRA.
 *
 * @since NRA 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function nra_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'nra' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'nra' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'nra' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'nra' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since NRA 1.1
 */
function nra_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'nra_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since NRA 1.0
 */
function nra_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'nra-fonts', nra_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );
	wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/js/fancybox/source/jquery.fancybox.css', array(), '3.2' );
	// wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/fancybox/source/jquery.fancybox.js', array(), '20141010', true );
	// wp_enqueue_script( 'fancybox-script2', get_template_directory_uri() . '/js/fancybox/source/jquery.fancybox.pack.js', array(), '20141010', true );
    //GOOGLE FONTS
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Montserrat|Libre+Baskerville:400italic', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'nra-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'nra-ie', get_template_directory_uri() . '/css/ie.css', array( 'nra-style' ), '20141010' );
	wp_style_add_data( 'nra-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'nra-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'nra-style' ), '20141010' );
	wp_style_add_data( 'nra-ie7', 'conditional', 'lt IE 8' );
    //APP.CSS
	wp_enqueue_style( 'nra-styles', get_template_directory_uri() . '/css/app.css', array( 'nra-style' ), '20160402' );

	wp_enqueue_script( 'nra-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

    //APP JS
	wp_enqueue_script( 'nra-main-script', get_template_directory_uri() . '/js/app.js', array(), '20160402', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'nra-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'nra-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'nra-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'nra' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'nra' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'nra_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since NRA 1.0
 *
 * @see wp_add_inline_style()
 */
function nra_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'nra-style', $css );
}
add_action( 'wp_enqueue_scripts', 'nra_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since NRA 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function nra_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'nra_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since NRA 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function nra_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'nra_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since NRA 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since NRA 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since NRA 1.0
 */
require get_template_directory() . '/inc/customizer.php';
show_admin_bar( false );

function news_post_type() {
    $labels = array(
        'name'              => _x('News', 'post type general name'),
        'singular_name'     => _x('New', 'post type singular name'),
        'add_new'           => _x('Add New', 'author'),
        'add_new_item'      => __('Add New News'),
        'edit_item'         => __('Edit News'),
        'new_item'          => __('New News'),
        'all_items'         => __('All News'),
        'view_item'         => __('View News'),
        'search_items'      => __('Search News'),
        'not_found'         => __('No news found'),
        'menu_name'         => __('News')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_position'     => 5,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'news' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'taxonomies' => array('category'),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail' )
    );
    register_post_type('news', $args);
}
function locations_post_type() {
    $labels = array(
        'name'              => _x('Locations', 'post type general name'),
        'singular_name'     => _x('Locations', 'post type singular name'),
        'add_new'           => _x('Add Locations', 'author'),
        'add_new_item'      => __('Add New Locations'),
        'edit_item'         => __('Edit Locations'),
        'new_item'          => __('New Locations'),
        'all_items'         => __('All Locations'),
        'view_item'         => __('View Locations'),
        'search_items'      => __('Search Locations'),
        'not_found'         => __('No Locations found'),
        'menu_name'         => __('Locations')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_position'     => 3,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'locations' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'taxonomies' => array('category'),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail' )
    );
    register_post_type('locations', $args);
}
function physicians_post_type() {
    $labels = array(
        'name'              => _x('Physicians', 'post type general name'),
        'singular_name'     => _x('Physician', 'post type singular name'),
        'add_new'           => _x('Add Physician', 'author'),
        'add_new_item'      => __('Add New Physician'),
        'edit_item'         => __('Edit Physicians'),
        'new_item'          => __('New Physicians'),
        'all_items'         => __('All Physicians'),
        'view_item'         => __('View Physicians'),
        'search_items'      => __('Search Physicians'),
        'not_found'         => __('No Physicians found'),
        'menu_name'         => __('Physicians')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_position'     => 6,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'physicians' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'taxonomies' => array('category'),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail' )
    );
    register_post_type('physicians', $args);
}
function midlevel_post_type() {
    $labels = array(
        'name'              => _x('Mid-Levels', 'post type general name'),
        'singular_name'     => _x('Mid-Level', 'post type singular name'),
        'add_new'           => _x('Add Mid-Level', 'author'),
        'add_new_item'      => __('Add New Mid-Level'),
        'edit_item'         => __('Edit Mid-Levels'),
        'new_item'          => __('New Mid-Levels'),
        'all_items'         => __('All Mid-Levels'),
        'view_item'         => __('View Mid-Levels'),
        'search_items'      => __('Search Mid-Levels'),
        'not_found'         => __('No Mid-Levels found'),
        'menu_name'         => __('Mid-Levels')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_position'     => 7,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'midlevel' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'taxonomies' => array('category'),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail' )
    );
    register_post_type('midlevel', $args);
}
function procedures_post_type() {
    $labels = array(
        'name'              => _x('Procedures', 'post type general name'),
        'singular_name'     => _x('Procedure', 'post type singular name'),
        'add_new'           => _x('Add Procedure', 'author'),
        'add_new_item'      => __('Add New Procedure'),
        'edit_item'         => __('Edit Procedures'),
        'new_item'          => __('New Procedures'),
        'all_items'         => __('All Procedures'),
        'view_item'         => __('View Procedures'),
        'search_items'      => __('Search Procedures'),
        'not_found'         => __('No Procedures found'),
        'menu_name'         => __('Procedures'),

    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_position'     => 8,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'procedures' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'taxonomies' => array('category'),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
    );
    register_post_type('procedures', $args);
}
function case_of_month_post_type() {
    $labels = array(
        'name'              => _x('Case of month', 'post type general name'),
        'singular_name'     => _x('Case', 'post type singular name'),
        'add_new'           => _x('Add Case', 'author'),
        'add_new_item'      => __('Add New Case'),
        'edit_item'         => __('Edit Case'),
        'new_item'          => __('New Cases'),
        'all_items'         => __('All Cases'),
        'view_item'         => __('View Cases'),
        'search_items'      => __('Search Cases'),
        'not_found'         => __('No Cases found'),
        'menu_name'         => __('Cases'),

    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_position'     => 9,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'cases' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'taxonomies' => array('category'),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
    );
    register_post_type('cases', $args);
}
add_action('init', 'news_post_type');
add_action('init', 'locations_post_type');
add_action('init', 'physicians_post_type');
add_action('init', 'midlevel_post_type');
add_action('init', 'procedures_post_type');
add_action('init', 'case_of_month_post_type');
function register_my_widgets(){
	register_sidebar( array(
		'name' => 'Sidebar at patients page',
		'id' => 'locations-sidebar',
		'description' => 'Sidebar at patients page',
		'before_widget' => '<div class="homepage-widget-block">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
    register_sidebar( array(
		'name' => 'Sidebar at Locations page',
		'id' => 'patients-sidebar',
		'description' => 'Sidebar at Locations page',
		'before_widget' => '<div class="homepage-widget-block">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
    register_sidebar( array(
		'name' => 'Sidebar at Contact page',
		'id' => 'contact-sidebar',
		'description' => 'Sidebar at Contact us page',
		'before_widget' => '<div class="homepage-widget-block">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => 'Sidebar at Procedures page',
		'id' => 'procedures-sidebar',
		'description' => 'Sidebar at Procedures page',
		'before_widget' => '<div class="homepage-widget-block">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => 'Sidebar at Physicians page',
		'id' => 'physicians-sidebar',
		'description' => 'Sidebar at Physicians page',
		'before_widget' => '<div class="homepage-widget-block">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => 'Sidebar at Mid-Level page',
		'id' => 'midlevel-sidebar',
		'description' => 'Sidebar at Mid-Level page',
		'before_widget' => '<div class="homepage-widget-block">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
}
add_action( 'widgets_init', 'register_my_widgets' );

add_action( 'wp_ajax_load_locations', 'load_locations_callback' );
add_action( 'wp_ajax_nopriv_load_locations', 'load_locations_callback' );

function load_locations_callback() {
	$zip = empty($_GET['zip']) ? '' : $_GET['zip'];
	$procedure = empty($_GET['procedure']) ? '' : $_GET['procedure'];
	$show_all = empty($_GET['show_all']) ? 0 : $_GET['show_all'];

	global $wpdb;

	if ($zip) {
		$location = $wpdb->get_var('select z2.location from zips z2 where z2.zip = "' . $zip . '"');
		if (!$location) {
			wp_die();
		}
	}
//var_dump($location);die();
	$select_query = '';
	$join_query = '';
	$where_query = '';
	$orderby_query = '';
	$limit_query = '';

	if (!$show_all) {
		$limit_query = ' Limit 0, 3';
	}

	if ($zip) {
		$select_query .= ', lat_lng_distance((select z2.location from zips z2 where z2.zip = "' . $zip . '"), COALESCE(z.location, PointFromText(\'POINT(0 0)\'))) as distance';
		$join_query .= ' left join ' . $wpdb->postmeta . ' pm on(p.ID = pm.post_id and pm.meta_key = "zip") left join zips z on (pm.meta_value = z.zip)';
		$orderby_query = ' ORDER by distance';
	}
	if ($procedure) {
		$join_query .= ' inner join ' . $wpdb->postmeta .	' pm2 on(p.ID = pm2.post_id and pm2.meta_key = "procedures")';
		$where_query .= ' and pm2.meta_value LIKE "%""' . $procedure . '""%"';
	}

	$sql = 'select SQL_CALC_FOUND_ROWS p.ID'  . $select_query . ' from ' . $wpdb->posts . ' p' . $join_query .
		' where p.post_type = "locations" and p.post_status = "publish"' . $where_query .
		$orderby_query . $limit_query;
//var_dump($sql);die();
/*
select SQL_CALC_FOUND_ROWS p.ID, ST_Distance((select z2.location from zips z2 where z2.zip = 42323), z.location) as distance from wp_posts p inner join wp_postmeta pm on(p.ID = pm.post_id) inner join zips z on (pm.meta_value = z.zip) where p.post_type = 'locations' and p.post_status = 'publish' and pm.meta_key = 'zip' ORDER by distance Limit 0, 3
*/
	$results = $wpdb->get_results($sql);
	if ($results) {
		$locs_count = $wpdb->get_var('SELECT FOUND_ROWS()');
		foreach ($results as $value) {
			$post_id = $value->ID;//var_dump($post_id);continue;
			include('location-item.php');
		} ?>
		<div class="view-all-block">
	  	<?php if (!$show_all && $locs_count > 3) : ?>
	  	<a href="javascript:void(0)" class="read-more" id="view-all" title="View All">View All</a>
		<?php endif; ?>
	  	</div>
<?php	}
	wp_die();
}

add_action( 'wp_ajax_load_physicians', 'load_physicians_callback' );
add_action( 'wp_ajax_nopriv_load_physicians', 'load_physicians_callback' );

function load_physicians_callback() {
	$cat = empty($_GET['cat']) ? '' : $_GET['cat'];
	$show_all = 1;//empty($_GET['show_all']) ? 0 : $_GET['show_all'];

	$args = array( 'post_type' => 'physicians');

	if ($show_all) {
		$args['no_paging'] = true;
		$args['posts_per_page'] = -1;
	} else {
		$args['posts_per_page'] = 3;
	}

	if ($cat) {
	    $args['tax_query'] = array(
	        array(
	            'taxonomy'     => 'physicians_category',
	            'field'   => 'term_id',
	            'terms' => $cat
	        ),
	    );
	}

    $loop = new WP_Query( $args );
    if ( $loop->have_posts()) :
    while ( $loop->have_posts() ) : $loop->the_post(); 
    get_template_part('physicians', 'item');
	endwhile;wp_reset_postdata();
   ?>
  	<div class="view-all-block">
  	<?php if (/*!$show_all && $loop->max_num_pages > 1*/0) : ?>
  	<a href="javascript:void(0)" class="read-more" id="view-all" title="View All">View All</a>
	<?php endif; ?>
  	</div>
  <?php endif;
	wp_die();
}

add_action( 'wp_ajax_load_midlevel', 'load_midlevel_callback' );
add_action( 'wp_ajax_nopriv_load_midlevel', 'load_midlevel_callback' );

function load_midlevel_callback() {
	$cat = empty($_GET['cat']) ? '' : $_GET['cat'];
	$show_all = 1;//empty($_GET['show_all']) ? 0 : $_GET['show_all'];

	$args = array( 'post_type' => 'midlevel');

	if ($show_all) {
		$args['no_paging'] = true;
		$args['posts_per_page'] = -1;
	} else {
		$args['posts_per_page'] = 3;
	}

	if ($cat) {
	    $args['tax_query'] = array(
	        array(
	            'taxonomy'     => 'midlevel_category',
	            'field'   => 'term_id',
	            'terms' => $cat
	        ),
	    );
	}

    $loop = new WP_Query( $args );
    if ( $loop->have_posts()) :
    while ( $loop->have_posts() ) : $loop->the_post(); 
    get_template_part('midlevel', 'item');
	endwhile;wp_reset_postdata();
   ?>
  	<div class="view-all-block">
  	<?php if (/*!$show_all && $loop->max_num_pages > 1*/0) : ?>
  	<a href="javascript:void(0)" class="read-more" id="view-all" title="View All">View All</a>
	<?php endif; ?>
  	</div>
  <?php endif;
	wp_die();
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Theme Options',
        'capability'    => 'manage_options',
        'parent_slug'   => 'options-general.php',
    ));
}

add_action( 'init', 'create_physicians_category' );
function create_physicians_category() {
	register_taxonomy(
		'physicians_category',
		'physicians',
		array(
			'label' => __( 'Categories' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_midlevel_category' );
function create_midlevel_category() {
	register_taxonomy(
		'midlevel_category',
		'midlevel',
		array(
			'label' => __( 'Categories' ),
			'hierarchical' => true,
		)
	);
}

function get_category_videos_template( $template ) {
  $category_ID = intval( get_query_var('cat') );
  $category = get_category( $category_ID );
  $parent_ID = $category->parent;
  $parent_category = get_category( $parent_ID );
  /**
   * Check wheter the $parent_category object is not a WordPress Error and its slug is 'videos'
   */
  if ( !is_wp_error( $parent_category ) AND $parent_category->slug == 'videos' ) {
    /**
     * Check if the template file exists
     *
     */
    if ( file_exists( TEMPLATEPATH . '/category-' . $parent_category->slug . '.php' )) {
      return TEMPLATEPATH . '/category-' . $parent_category->slug . '.php';
    }
  } else {
    return $template;
  }
}
add_filter( 'category_template', 'get_category_videos_template' );
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
// Breadcrumbs
function custom_breadcrumbs() {

	// Settings
	$separator          = '&gt;';
	$breadcrums_id      = 'breadcrumbs';
	$breadcrums_class   = 'breadcrumbs';
	$home_title         = 'Homepage';

	// If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
	$custom_taxonomy    = 'product_cat';

	// Get the query & post information
	global $post,$wp_query;

	// Do not display on the homepage
	if ( !is_front_page() ) {

		// Build the breadcrums
		echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

		// Home page
		echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
		echo '<li class="separator separator-home"> ' . $separator . ' </li>';

		if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

			echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

		} else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

			// If post is a custom post type
			$post_type = get_post_type();

			// If it is a custom post type display name and link
			if($post_type != 'post') {

				$post_type_object = get_post_type_object($post_type);
				$post_type_archive = get_post_type_archive_link($post_type);

				echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" ' . ' title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';

			}

			$custom_tax_name = get_queried_object()->name;
			echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

		} else if ( is_single() ) {

			// If post is a custom post type
			$post_type = get_post_type();

			// If it is a custom post type display name and link
			if($post_type != 'post') {

				$post_type_object = get_post_type_object($post_type);
				$post_type_archive = get_post_type_archive_link($post_type);
				$href = '';
				$tytle = '';
				switch ($post_type) {
					case 'procedures':
						$href = get_site_url().'/procedures-specialities/';
						break;
					case 'physicians':
						$href = get_site_url().'/our-physicians/';
						$tytle = 'Our ';
						break;
					case 'midlevel':
						$href = get_site_url().'/midlevels/';
						$tytle = 'Our ';
						break;
					default:
						$href = get_site_url();
						break;
				}

				echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a href="'.$href.'" class="bread-cat bread-custom-post-type-' . $post_type . '" ' . ' title="' . $post_type_object->labels->name . '">' . $tytle . $post_type_object->labels->name . '</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';

			}

			// Get post category info
			$category = get_the_category();

			if(!empty($category)) {

				// Get last category post is in
				$last_category = end(array_values($category));

				// Get parent any categories and create array
				$get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
				$cat_parents = explode(',',$get_cat_parents);

				// Loop through parent categories and store in variable $cat_display
				$cat_display = '';
				foreach($cat_parents as $parents) {
//					$cat_display .= '<li class="item-cat">'.$parents.'</li>';
//					$cat_display .= '<li class="separator"> ' . $separator . ' </li>';
				}

			}

			// If it's a custom post type within a custom taxonomy
			$taxonomy_exists = taxonomy_exists($custom_taxonomy);
			if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

				$taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
				$cat_id         = $taxonomy_terms[0]->term_id;
				$cat_nicename   = $taxonomy_terms[0]->slug;
				$cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
				$cat_name       = $taxonomy_terms[0]->name;

			}

			// Check if the post is in a category
			if(!empty($last_category)) {
				echo $cat_display;
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

				// Else if post is in a custom taxonomy
			} else if(!empty($cat_id)) {

				echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" ' . ' title="' . $cat_name . '">' . $cat_name . '</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

			} else {

				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

			}

		} else if ( is_category() ) {

			// Category page
			echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

		} else if ( is_page() ) {

			// Standard page
			if( $post->post_parent ){

				// If child page, get parents
				$anc = get_post_ancestors( $post->ID );

				// Get parents in the right order
				$anc = array_reverse($anc);

				// Parent page loop
				foreach ( $anc as $ancestor ) {
					$parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" '  . ' title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
					$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
				}

				// Display parent pages
				echo $parents;

				// Current page
				echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

			} else {

				// Just display current page if not parents
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

			}

		} else if ( is_tag() ) {

			// Tag page

			// Get tag information
			$term_id        = get_query_var('tag_id');
			$taxonomy       = 'post_tag';
			$args           = 'include=' . $term_id;
			$terms          = get_terms( $taxonomy, $args );
			$get_term_id    = $terms[0]->term_id;
			$get_term_slug  = $terms[0]->slug;
			$get_term_name  = $terms[0]->name;

			// Display the tag name
			echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

		} elseif ( is_day() ) {

			// Day archive

			// Year link
			echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" ' . ' title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

			// Month link
			echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" ' . ' title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';

			// Day display
			echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

		} else if ( is_month() ) {

			// Month Archive

			// Year link
			echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" ' . ' title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

			// Month display
			echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

		} else if ( is_year() ) {

			// Display year archive
			echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

		} else if ( is_author() ) {

			// Auhor archive

			// Get the author information
			global $author;
			$userdata = get_userdata( $author );

			// Display author name
			echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

		} else if ( get_query_var('paged') ) {

			// Paginated archives
			echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

		} else if ( is_search() ) {

			// Search results page
			echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';

		} elseif ( is_404() ) {

			// 404 page
			echo '<li>' . 'Error 404' . '</li>';
		}

		echo '</ul>';

	}

}

function getExcerpt($str, $length = 250, $more = '...'){
    $s = '';

    if($str){
        $s.= substr($str, 0, $length);
    }

    if(strlen($str) > $length){
        $s.=$more;
    }

    return $s;
}

?>