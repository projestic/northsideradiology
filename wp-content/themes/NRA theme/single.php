<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header();global $wp_query;
get_template_part('head-section'); ?>

    <section class="section-physicians section-patient">
        <div class="container">
            <div class="breadcrumbs">
                <?php custom_breadcrumbs(); ?>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post(); 
                        	if($post->post_content != "") :
                    ?>
					<div class="abstract-text">
                        <?php
                           echo get_the_content(); 
                           endif;
                        // End the loop.
                       ?>
                    </div>
                    <?php endwhile; ?>
                    
					<?php if (get_field('media_box')){?>
	                    <div class="media-box">
	                        <?php the_field('media_box');?>
	                    </div>
					<?php }?>

					<?php if (get_field('second_text_block')){?>
	                    <div class="user-text">
	                        <?php the_field('second_text_block');?>
	                    </div>
	                <?php }?>

                    <?php
                        if( have_rows('related_pages') ):
                    ?>
                        <div class="block-title">
                            <span class="text"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon.png"></span>
                        </div>
                        <div class="related_pages_block">
                            <h4>Related Pages</h4>
                            <ul class="related_pages_list">
                                <?php

                                    // check if the repeater field has rows of data
                                    if( have_rows('related_pages') ):

                                        // loop through the rows of data
                                        while ( have_rows('related_pages') ) : the_row();
                                            $post_object = get_sub_field('related_post');
                                            $post = $post_object;
                                           setup_postdata( $post ); ?>
                                    <li><span class="title"><?php the_title();?></span><a href="<?php the_permalink();?>" class="more-btn">View ></a></li>
                                    <?php wp_reset_postdata();

                                        endwhile;

                                    else :

                                        // no rows found

                                    endif;

                                    ?>
                            </ul>
                        </div>
                    <?php endif;?>
                </div>
                <div class="col-md-3">
                    <?php
                        if ( function_exists('dynamic_sidebar') )
                            dynamic_sidebar('locations-sidebar');
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
