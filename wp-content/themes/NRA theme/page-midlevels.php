<?php
/**
 * Template Name: MidLevel Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header();
?>
<?php while (have_posts()) : the_post(); ?>
    <?php
    /*$specialties_info = get_field_object('field_56af3644ebf79');
    $specialties_arr = $specialties_info['choices'];*/

    $cats = get_terms('midlevel_category', array('hide_empty' => 0));
    $args = array(
        'post_type' => 'midlevel', 
        'no_paging' => true, 
        'posts_per_page' => -1,
        'meta_key'       => 'user_last_name',
        'orderby'        => 'meta_value',
        'order'          => 'ASC'
    );

    ?>
    <?php get_template_part('head-section'); ?>
    <section class="section-physicians">
        <div class="container">

            <div class="breadcrumbs">
                <?php custom_breadcrumbs(); ?>
            </div>

            <div class="abstract-text">
                <?php the_content(); ?>
            </div>

            <div class="media-box">
                <?php the_field('media_block'); ?>
            </div>

            <div class="profile-list" id="physicians-list">
                <div class="row row-physicians">
                    <?php $loop = new WP_Query($args);
                    if ($loop->have_posts()) :
                        while ($loop->have_posts()) : $loop->the_post();

                            get_template_part('midlevel', 'item');
    
                        endwhile;
                        wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <!-- <div class="view-all-block"> -->
                <?php if (/*$loop->max_num_pages > 1*/0) : ?>
                    <!-- <a href="#" class="read-more" id="view-all" title="View All">View All</a> -->
                <?php endif; ?>
            <!-- </div> -->
        </div>
    </section>
<?php endwhile; ?>
    <script>
        var cat = "";
        var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
        jQuery(function ($) {
            $('.filter-box .filter-spec').click(function(e) {
                e.preventDefault();
                if (!$(this).hasClass('active')) {
                    $('.filter-box .filter-spec').removeClass('active');
                    $(this).toggleClass('active');
                    cat = $(this).data('spec');
                    load_mid_levels(1);
                }                
            });
//                $('#physicians-list').on('click', '#view-all', function (e) {
//                    e.preventDefault();
//                    load_mid_levels(1);
//                    return false;
//                });

            

            function load_mid_levels(all) {
                var data = {'action': 'load_mid_levels', cat: cat, show_all: all};
                jQuery.get(ajax_url, data, function (response) {
                    console.log('clicked');
                    if (response) {
                        $('#physicians-list').html(response);
                    } else {
                        $('#physicians-list').html('<div class="abstract-text"><p class="noresult">There are no items</p></div><div class="view-all-block"></div>');
                    }
                });
            }
        });
    </script>
<?php get_footer(); ?>