<?php
/**
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header();global $wp_query;
?>

    <section class="head-section">
        <div class="container-fluid">
            <h1 class="page-title">Our Physicians</h1>
        </div>
    </section>

    <section class="section-physicians section-patient">
        <div class="container">
            <div class="breadcrumbs">
                <?php custom_breadcrumbs(); ?>
            </div>
            <div class="row">
                <div class="col-md-9">
                        <h3 class="title"><?php the_title();?></h3>

                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="photo-holder">
                                    <?php get_template_part('profile-thumbnail', 'item'); ?>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="text-holder">
                                    <?php
                                        // Start the loop.
                                        while ( have_posts() ) : the_post(); 
                                        if($post->post_content != "") :
                                    ?>
                                        <div class="abstract-text">
                                            <?php  echo get_the_content(); ?>
                                        </div>
                                    <?php endif;?>
                                    <?php endwhile; ?>
                                    
                                    <?php if (get_field('media_box')){?>
                                        <div class="media-box">
                                            <?php the_field('media_box');?>
                                        </div>
                                    <?php }?>
                                    <?php if (get_field('second_text_block')){?>
                                        <div class="user-text">
                                            <?php the_field('second_text_block');?>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>

                        </div>

                    <?php
                        if( have_rows('related_pages') ):
                    ?>
                        <div class="block-title">
                            <span class="text"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon.png"></span>
                        </div>
                        <div class="related_pages_block">
                            <h4>Related Pages</h4>
                            <ul class="related_pages_list">
                                <?php

                                    // check if the repeater field has rows of data
                                    if( have_rows('related_pages') ):

                                        // loop through the rows of data
                                        while ( have_rows('related_pages') ) : the_row();
                                            $post_object = get_sub_field('related_post');
                                            $post = $post_object;
                                           setup_postdata( $post ); ?>
                                    <li><span class="title"><?php the_title();?></span><a href="<?php the_permalink();?>" class="more-btn">View ></a></li>
                                    <?php wp_reset_postdata();

                                        endwhile;

                                    else :

                                        // no rows found

                                    endif;

                                    ?>
                            </ul>
                        </div>
                    <?php endif;?>
                </div>
                <div class="col-md-3">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar at Physicians page') ) : ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
<?php get_footer(); ?>
