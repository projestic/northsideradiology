<?php
/**
 * Template Name: Locations Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

global $wp_query;
get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <?php
    $args = array('post_type' => 'locations', 'posts_per_page' => 3);
    ?>
    <?php get_template_part('head-section'); ?>

    <section class="section-physicians section-locations">
        <div class="container">
            <div class="breadcrumbs">
                <?php custom_breadcrumbs(); ?>
            </div>
            <div class="row location-row">

                <div class="col-md-9">
                    <div class="map_holder" id="map"></div>

                    <div class="block-title">
                        <span class="text"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon.png"></span>
                    </div>
                    <div class="adress_controls_wrp clearfix">
                        <span>Find a location near me</span> <input type="text" class="zipcode" placeholder="zip code" id="zip-code" value="<?php echo $zip; ?>">
                        <!--                                <a href="#" class="procedure_sort btn btn-default full-width up"><span>Sort by PROCEDURES</span> <i class="fa fa-angle-up"></i> <i class="fa fa-angle-down"></i></a>-->
                        <?php
                        $fields = get_field_object('field_56af30c9c597a');
                        ?>
                        <label class="procedure_label up">
                            <select class="procedure_sort btn btn-default full-width" id="procedure-sort">
                                <option value="none" disabled selected>Sort by PROCEDURES</option>
                                <option value=" ">View All</option>
                                <?php foreach ($fields['choices'] as $v): ?>
                                    <option
                                        value="<?php echo $v ?>" <?php echo $v == $procedure ? 'selected' : ''; ?> ><?php echo $v ?></option>
                                <?php endforeach; ?>
                            </select>
                        </label>
                    </div>
                    <div class="profile-list" id="locations-list">
                        <?php
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) :
                            while ($loop->have_posts()) : $loop->the_post();
                                $post_id = get_the_ID();
                                get_template_part('location', 'item');
                            endwhile;
                            wp_reset_postdata();
                            ?>
                            <div class="view-all-block">
                                <a href="javascript:void(0)" class="read-more" id="view-all" title="View All">View
                                    All</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <?php
                    if (function_exists('dynamic_sidebar'))
                        dynamic_sidebar('patients-sidebar');
                    ?>

                </div>
            </div>


        </div>
    </section>
<?php endwhile; ?>
<?php
$args2 = array('post_type' => 'locations', 'posts_per_page' => -1);
$loop = new WP_Query($args2);
if ($loop->have_posts()) : ?>
    <div id="locations-listing" hidden>
        <?php while ($loop->have_posts()) : $loop->the_post();
            $post_id = get_the_ID(); ?>
            <p><?php $location = get_field('location_map');
                echo $location['lat'] . '|' . $location['lng'] . '|' . $location['address'] ?></p>
        <?php endwhile; ?>
    </div>
<?php wp_reset_postdata(); endif; ?>

<script>
    var zip = "";
    var procedure = "";
    var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
    jQuery(function ($) {
        $('#zip-code').keypress(function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                if (/^\d{5}$/.test(this.value) || this.value == '') {
                    zip = this.value;
                    load_locations(0);
                } else {
                    alert('Invalid zip code');
                }
            }

        });

        $('#procedure-sort').change(function () {
            if (this.value) {
                if (this.value === ' ') {
                    procedure = this.value;
                    load_locations(1);
                    this.value === '';
                    $(this).find('option').first().attr('selected', true);
                } else {
                    procedure = this.value;
                    load_locations(0);
                }
            }
        });

        $('#locations-list').on('click', '#view-all', function (e) {
            e.preventDefault();
            load_locations(1);
            return false;
        });

        function load_locations(all) {
            var data = {'action': 'load_locations', zip: zip, procedure: procedure, show_all: all};
            jQuery.get(ajax_url, data, function (response) {
                if (response) {
                    $('#locations-list').html(response);
                } else {
                    $('#locations-list').html('<h3>There are no items</h3>');
                }
            });
        }
    });
</script>
<?php get_footer(); ?>