<?php
/**
 * Template Name: Index Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="main_block scrollable">
        <div class="container-fluid">
            <div class="main-blocks clearfix">
                <div class="col-md-8 col-sm-12 big_block">
                    <div class="contact-block block" style="background: url('<?php the_field('image_block_1')?>') no-repeat; background-size: cover;">
                        <div class="text-holder">
                            <h2><?php the_field('text_block_1'); ?></h2>
                            <a href="<?php echo get_permalink(get_field('link_block_1'))?>" class="btn main-btn"><?php the_field('link_text_block_1'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <?php
                        $image = wp_get_attachment_image_src(get_field('image_block_2'), 'full');
                        ?>
                    <div class="uterine block"  <?php if ($image) : ?>style="background:url(<?php echo $image[0]; ?>) no-repeat;"<?php endif; ?>>                        
                        <div class="text-holder">
                            <h2><?php the_field('text_block_2'); ?></h2>
                            <a href="<?php echo get_permalink(get_field('link_block_2'))?>" class="btn main-btn"><?php the_field('link_text_block_2'); ?></a>
                        </div>
                    </div>
                    <?php
                        $image = wp_get_attachment_image_src(get_field('image_block_3'), 'full');
                        ?>
                    <div class="care block" <?php if ($image) : ?>style="background:url(<?php echo $image[0]; ?>) no-repeat;"<?php endif; ?>>
                        <div class="text-holder">
                            <h2><?php the_field('text_block_3'); ?></h2>
                            <a href="<?php echo get_permalink(get_field('link_block_3'))?>" class="btn main-btn"><?php the_field('link_text_block_3'); ?></a>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>
            <div class="links-block">
                <h3 class="block-title">
               <span class="text"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon.png"></span>
            </h3>
            <?php if (have_rows('featured_pages')) : ?>
                        
                <ul class="links-list animated clearfix">
                <?php while ( have_rows('featured_pages') ) : the_row(); ?>
                    <li class="animated">
                        <div>
                            <?php the_sub_field('icon'); ?>
                            <h4><?php the_sub_field('title'); ?></h4>
                            <p><?php the_sub_field('description'); ?></p>
                            <a href="<?php echo get_permalink(get_sub_field('link'))?>" class="more-btn">Read more ></a>
                        </div>
                    </li>
                <?php endwhile; ?>
                </ul>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="content_block">
        <div class="container-fluid">
            <div class="welcome_block">
                <h3 class="block-title"><span class="text">Welcome</span></h3>
                <p>
                    <?php echo wp_strip_all_tags(get_the_content());                        ?>
                </p>
            </div>
            <div class="procedures_block">
                <h3 class="block-title"><spa class="text">PROCEDURES</spa></h3>
                <ul class="procedures-list">
                    <?php if (have_rows('featured_procedures')) :
                        global $post;
                        $global_post = $post;
                        while ( have_rows('featured_procedures', $global_post->ID) ) : the_row();
                        $post = get_sub_field('procedure');
                        setup_postdata( $post ); ?>
                        <li class="animated" >
                            <div>
                                <h4><?php the_title();?></h4>
                                <p><?php echo wp_strip_all_tags(get_the_excerpt());?></p>
                                <a href="<?php the_permalink();?>" class="more-btn">Read More ></a>
                            </div>
                        </li>
                        <?php  endwhile;  wp_reset_postdata(); $post = $global_post; endif; ?>
                </ul>
                <a href="<?php echo get_permalink(get_field('link_page'))?>" class="procedures">View all Procedures</a>
            </div>
            <div class="news_block">
                <h3 class="block-title"><span class="text">News</span></h3>
                <ul class="news_list">
                    <?php $args = array( 'post_type' => 'news', 'posts_per_page' => 3,);
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <li>
                            <h4 class="news_title"><?php the_title();?></h4>
                            <a href="<?php echo get_permalink()?>" class="more-btn">Read More ></a>
                        </li>
                        <?php  endwhile; wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
    <?php get_footer(); ?>