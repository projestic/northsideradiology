<?php
/**
 * @package WordPress
 * @subpackage NRA
 * @since NRA 1.0
 */

get_header();
global $wp_query;
get_template_part('head-section'); ?>

<section class="section-physicians section-patient">
    <div class="container">
        <div class="breadcrumbs">
            <?php custom_breadcrumbs(); ?>
        </div>
        <div class="row">
            <div class="col-md-9">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    if ($post->post_content != "") :
                        ?>
                        <div class="abstract-text">
                            <?php
                            echo get_the_content(); ?>

                        </div>
                    <?php endif; // End the loop. ?>
                <?php endwhile; ?>

                <?php if (get_field('media_box')) { ?>
                    <div class="media-box">
                        <?php the_field('media_box'); ?>
                    </div>
                <?php } ?>

                <?php if (get_field('second_text_block')) { ?>
                    <div class="user-text">
                        <?php the_field('second_text_block'); ?>
                    </div>
                <?php } ?>

                <?php
                if (have_rows('related_pages')):
                    ?>
                    <div class="block-title">
                        <span class="text"><img
                                src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon.png"></span>
                    </div>
                    <div class="related_pages_block">
                        <h4>Related Pages</h4>
                        <ul class="related_pages_list">
                            <?php

                            // check if the repeater field has rows of data
                            if (have_rows('related_pages')):

                                // loop through the rows of data
                                while (have_rows('related_pages')) : the_row();
                                    $post_object = get_sub_field('related_post');
                                    $post = $post_object;
                                    setup_postdata($post); ?>
                                    <li><span class="title"><?php the_title(); ?></span><a
                                            href="<?php the_permalink(); ?>" class="more-btn">View ></a></li>
                                    <?php wp_reset_postdata();

                                endwhile;

                            else :

                                // no rows found

                            endif;

                            ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-3">
                <!-- <?php get_template_part('dinamic-sidebar'); ?> -->
                <?php
                $page_id = $wp_query->post->ID;
                if ($page_id == 79) :
                    ?>
                    <div class="homepage-widget-block">
                        <h4>Cases of the month</h4>
                        <div class="cases-select-holder">
                            <select class="cases-select"
                                    onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                                <option value="0" disabled selected>Select a month</option>
                                <?php
                                $type = 'cases';
                                $args3 = array(
                                    'post_type' => $type,
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'caller_get_posts' => 1,
                                    'order' => 'ASC'
                                );
                                $my_query = null;
                                $my_query = new WP_Query($args3);
                                if ($my_query->have_posts()) {
                                    while ($my_query->have_posts()) : $my_query->the_post(); ?>
                                        <option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>
                                        <?php
                                    endwhile;
                                }
                                wp_reset_query();  // Restore global post data stomped by the_post().
                                ?>
                            </select>
                            <span class="select-arrow"></span>
                        </div>

                    </div>
                <?php endif; ?>
                <?php dynamic_sidebar(); ?>
            </div>
        </div>
</section>
<?php get_footer(); ?>
