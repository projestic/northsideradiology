<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define('WP_ALLOW_REPAIR', true);
define('FS_METHOD', 'direct');
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'northsideradiology');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N+(|)*aMNo]T5f2ZEtPN|Ra(f:j7J.n2}~+6 5r>p0J~__PZm%-&}dl^`&pn-(I-');
define('SECURE_AUTH_KEY',  'PRl9/bRYCfy-3YjH4!/27:,;CXUScCBLW;xh$N9Ml&s|~1v|-r[itR(;Mluo1fXs');
define('LOGGED_IN_KEY',    'QM)3Xq}FP6q!T%7lruC`-qUEr#!-Og_;?OCCy1:ok*^5?uSLY;}I {H|=&U[~P`_');
define('NONCE_KEY',        '[ODAmb:Q-97Ep!y .ehVh:|F}HEil<D|Ohj(.XVEM)b);`Uj{qzx94MA,0P-pjx)');
define('AUTH_SALT',        'CQ(l86-N.gR a35YS:]uO#HK[--FSB-$87Xbe+MH  @qOq?P#LZfP&rx;q1+k_Mj');
define('SECURE_AUTH_SALT', '_[Mj7=<*:vL#|Oq}.P.`QXp.P+G1h^@nt6w|-W;}Nm/f@WZ?>1l|KR1>[FR;^,]N');
define('LOGGED_IN_SALT',   'ocF|}.#{Erh>6*$7xBfP$PfG}Jx{w?7!:NqY`u:dX5D;nHDCR)HKp2!fuEasNtMA');
define('NONCE_SALT',       'V>l|o(D&iJbU= B.t iVa*dH<N/r^$[~R4$cY7.Si)$/p!F-V$=_lB]P2sX[B7z6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

#Custom HyperDB Settings
define('DB_SLAVE_1', 'db02.aws.1path.com');
